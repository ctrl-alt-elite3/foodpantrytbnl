package edu.trincoll.foodpantrytbnl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodPantryTbnlApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodPantryTbnlApplication.class, args);
    }

}
