package edu.trincoll.foodpantrytbnl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FreeBytesController {
    private final List<String> teamMembers =
            List.of("Shashwath (Shash) Sunkum", "Anupam Khargharia", "George Zack", "Mia Creane", "Meti Habtegiorgis");

    @GetMapping("/free-bytes")  // localhost:8080/free-bytes
    public List<String> getTeamMembers() {
        return teamMembers;
    }
}
