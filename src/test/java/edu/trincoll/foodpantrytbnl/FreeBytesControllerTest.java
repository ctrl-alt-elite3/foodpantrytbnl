package edu.trincoll.foodpantrytbnl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FreeBytesControllerTest {
    private final List<String> expectedMembers =
            List.of("Shashwath (Shash) Sunkum", "Anupam Khargharia",
                    "George Zack", "Mia Creane", "Meti Habtegiorgis");

    @Test  // Simple unit test
    void getTeamMembers() {
        FreeBytesController dexMController = new FreeBytesController();
        assertAll(
                () -> assertThat(dexMController.getTeamMembers().size()).isEqualTo(5),
                () -> assertThat(dexMController.getTeamMembers()).containsAll(expectedMembers)
        );
    }

    @Test // Integration test
    void getTeamMembers(@Autowired TestRestTemplate restTemplate) {
        // Simpler but gives raw parameterized type warning
        // ResponseEntity<List> entity = restTemplate.getForEntity("/dexm", List.class);

        // More complex but avoids warning
        ResponseEntity<List<String>> typedEntity = restTemplate.exchange(
                "/free-bytes",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {}
        );
        List<String> body = typedEntity.getBody();
        assertThat(body).isNotNull();
        assertAll(
                () -> assertThat(typedEntity.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(body.size()).isEqualTo(5),
                () -> assertThat(body).containsAll(expectedMembers)
        );
    }
}